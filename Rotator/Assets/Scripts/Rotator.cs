using System;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float RotationSpeed { get; private set; }
    public float Angle => RotationSpeed * Time.deltaTime;

    private void Start()
    {
        SetRotationSpeed(30f);
    }

    private void Update()
    {
        RotateAroundCenterObject(this.gameObject, Vector3.up, Angle);
    }

    public void RotateAroundCenterObject(GameObject gameObject, Vector3 worldAxis, float angle)
    {
        var pivot = GetObjectPivot(gameObject);
        gameObject.transform.RotateAround(pivot, worldAxis, angle);
    }

    public Vector3 GetObjectPivot(GameObject gameObject)
    {
        var renderer = gameObject.GetComponent<Renderer>();

        if (renderer != null)
        {
            return renderer.bounds.center;
        }
        else
        {
            return gameObject.transform.position;
        }
    }

    public void SetRotationSpeed(float speedValue)
    {
        RotationSpeed = speedValue;
    }
}
