using UnityEngine;

public class SceneSetup : MonoBehaviour
{
    private readonly string OBJECT_NAME_CUBE = "Cube";

    void Start()
    {
        SetupInitialObjects();
    }

    private void SetupInitialObjects()
    {
        var cubeObject = new GameObject(OBJECT_NAME_CUBE);
        cubeObject.transform.SetPositionAndRotation(new Vector3(0f, 0f, 0.7f), Quaternion.Euler(30f, 45f, 0f));
        cubeObject.AddComponent<Rotator>();

        var cubeMesh = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubeMesh.transform.SetParent(cubeObject.transform);
        cubeMesh.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        cubeMesh.transform.localPosition = new Vector3(0.2f, 0.3f, 0f);
        cubeMesh.transform.localRotation = Quaternion.Euler(0f, 90f, 0f);
    }
}
