using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools.Utils;
using Assert = NUnit.Framework.Assert;

public class RotatorTest
{
    private GameObject testObject;
    private Rotator rotator;
    private float rotationSpeed = 30;

    [SetUp]
    public void Setup()
    {
        testObject = new GameObject("TestObject");
        rotator = testObject.AddComponent<Rotator>();

        rotator.SetRotationSpeed(rotationSpeed); 
    }

    [Test]
    public void RotateAroundCenterObject_RotatesGameObject()
    {
        var initialRotation = testObject.transform.rotation;

        rotator.RotateAroundCenterObject(testObject, Vector3.up, rotator.Angle);

        var actualRotation = testObject.transform.rotation;
        var expectedRotation = Quaternion.AngleAxis(rotator.Angle, Vector3.up) * initialRotation;
        Assert.That(actualRotation, Is.EqualTo(expectedRotation).Using(QuaternionEqualityComparer.Instance));
    }

    [Test]
    public void GetObjectPivot_ReturnsCorrectCenterForRenderer()
    {
        var renderer = testObject.AddComponent<MeshRenderer>();
        
        var actualCenter = rotator.GetObjectPivot(testObject);

        var expectedCenter = renderer.bounds.center;
        Assert.AreEqual(expectedCenter, actualCenter);
    }

    [Test]
    public void GetObjectPivot_ReturnsCorrectCenterForNonRenderer()
    {
        var actualCenter = rotator.GetObjectPivot(testObject);

        var expectedCenter = testObject.transform.position;
        Assert.AreEqual(expectedCenter, actualCenter);
    }


    [Test]
    public void RotateAroundCenterObject_RotationSpeedNotChanged()
    {
        rotator.RotateAroundCenterObject(testObject, Vector3.up, rotator.Angle);
        var actualRotationSpeed = rotator.RotationSpeed;

        Assert.That(actualRotationSpeed, Is.EqualTo(rotationSpeed));
    }
}
